# AIKEN NODE TEMPLATE
## simple node-js template with passport (local/jwt strategy) authentication (using MongoDB)

## HOW TO CONFIGURE
### modify server/config/main.js with your own settings  

## HOW TO RUN
### FIRST install npm
### install all dependencies
  > npm install
### edit the server/config/main file with your configuration
### run the Server
  > npm run build_server

### if no react client is needed then
  * delete client folder
  * in package.json
    * remove scripts.client;
    * remove \"npm run client\" from scripts.dev
    *  remove scripts.build_react and \"npm run build_react\" from scripts.production
  * in server/app/index.js
    * revome the REACT CLIENT commented part
