const express = require('express');
var bodyParser = require('body-parser');
var config = require('./server/config/main');

//initialize app
const app = express();

const port = process.env.PORT || 5000;

//middleware to parse json POST input
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json())

//passport authentication Strategy
const passport = require('./server/config/passport')

//initialize socket
const io = require('./server/config/socket')(app);

// getting-started.js
var mongoose = require('mongoose');
mongoose.connect(config.db);


var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
  console.log("db connected");
});

//initialize default routes
require('./server/app')(app);

//initialize auth roots
require('./server/app/auth')(app,passport);


app.listen(port, "127.0.0.1", () => console.log(`Listening on port ${port}`));
