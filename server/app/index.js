const express = require('express');
module.exports = function(app){

  // REACT CLIENT
  if(process.env.NODE_ENV=="production"){
    app.use('/', express.static(`${__dirname}/client/build`));
  }
  // REACT CLIENT END

  app.get('/api/hello', (req, res) => {
    res.send({ express: 'Hello From Express'});
  });

}
