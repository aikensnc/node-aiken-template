//import User model
var User = require('../schema/user');
var config = require('../config/main');
var jwt = require('jsonwebtoken');

module.exports = function(app,passport){

  //register user
  app.post('/api/signup', function(req, res) {
    if(!req.body.username || !req.body.password) {
      res.json({ success: false, message: 'Please enter email and password.' });
    } else {
      var newUser = new User({
        username: req.body.username,
        password: req.body.password
      });

      // Attempt to save the user
      newUser.save(function(err) {
        if (err) {
          console.log(err);
          return res.json({ success: false, message: 'That email address already exists.'});
        }
        res.json({ success: true, message: 'Successfully created new user.' });
      });
    }
  });

  //login
  app.post('/api/login', function(req, res) {
    User.findOne({
      username: req.body.username
    }, function(err, user) {
      if (err) throw err;

      if (!user) {
        res.send({ success: false, message: 'Authentication failed. User not found.' });
      } else {
        // Check if password matches
        user.comparePassword(req.body.password, function(err, isMatch) {
          if (isMatch && !err) {
            console.log(user);
            // Create token if the password matched and no error was thrown
            var token = jwt.sign({ id: user._id, username: user.username }, config.secret, {
              expiresIn: 10080 // 3 hours in seconds
            });
            var refreshToken = jwt.sign({ id: user._id }, config.secret, {
              expiresIn: 2592000 // 1 month in seconds
            });

            res.json({ success: true, token: token, refreshToken: refreshToken });
          } else {
            res.send({ success: false, message: 'Wrong Username or Password.' });
          }
        });
      }
    });
  });

  //active user
  app.get('/api/user', passport.authenticate('jwt', { session: false }), function(req, res) {
    res.json(req.user);
  });
}
