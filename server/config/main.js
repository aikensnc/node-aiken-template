module.exports = {
  secret: process.env.SECRET||'kp1tyokfHEDeP4zHLNNzmmagI',
  db: process.env.MONGO_DB||'mongodb://localhost/aikenTest',
  socketPort: 8000
};
