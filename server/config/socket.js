var http = require('http');
var socket = require('socket.io');
const config = require ('./main');
module.exports = function(App){
  const server = http.Server(App);
  const io = socket(server);
  io.on('connection', function(socket){
    console.log('a user connected');
    socket.on('disconnect', function(){
      console.log('user disconnected');
    });
    socket.on('message',function(msg) {
      console.log(msg);
    });

  });

  io.listen(config.socketPort);

  return io;
}
