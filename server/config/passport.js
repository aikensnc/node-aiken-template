//passport.js
const passport = require('passport');
const JwtStrategy = require('passport-jwt').Strategy;
const UserModel = require('../schema/user');
var config = require('./main');
const ExtractJwt = require('passport-jwt').ExtractJwt;

passport.serializeUser(function(user, done) {
  done(null, user.id);
});
passport.deserializeUser(function(id, done) {
  UserModel.findById(id, function(err, user) {
    done(err, user);
  });
});

var opts = {}
opts.jwtFromRequest = ExtractJwt.fromAuthHeaderWithScheme("JWT");
opts.secretOrKey = config.secret;

passport.use(new JwtStrategy(opts, function(jwt_payload, done) {
  UserModel.findOne({id: jwt_payload.id}, function(err, user) {
    if (err) {
      console.log(err);
      return done(err, false);
    }
    if (user) {
      done(null, user);
    } else {
      console.log("user not found");
      done(null, false);
    }
  });
}));

module.exports = passport;
